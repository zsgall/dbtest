package uitls;

import model.MasterData;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

public class DataFileWriter {

    private static String path = "";
    private static DecimalFormat decimalFormat = new DecimalFormat("0.000", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    public static void setPath(String aPath) {
        path = aPath + "/dbFiles";

        try {
            FileUtils.cleanDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static <T extends MasterData> void createRelations(List<T> list, String toConnection, String relationType) throws NoSuchFieldException, IOException, IllegalAccessException {

        String className = list.get(0).getClass().getSimpleName();
        Field field = list.get(0).getClass().getField(toConnection);

        File csvFile = new File(path + "/" + className + "_" + toConnection + ".csv");

        System.out.println(String.format("Creating relation %s ...", relationType));
        FileWriter writer = new FileWriter(csvFile, false);
        PrintWriter out = new PrintWriter(writer);

        out.write("\":START_ID\";\":END_ID\"\n");

        for (T item: list) {
            MasterData to = (MasterData) field.get(item);
            out.write(String.format("\"%s\";\"%s\"",item.id, to.id ));
            out.write('\n');
        }

        out.close();
    }

    public static <T extends MasterData> void write(List<T> list, Class clazz) throws IOException, IllegalAccessException {
        Field[] methods = clazz.getFields();

        File csvFile = new File(path + "/" + clazz.getSimpleName() + ".csv");

        System.out.println(String.format("Creating %s ...", csvFile.getAbsolutePath()));

        FileWriter writer = new FileWriter(csvFile, false);

        PrintWriter out = new PrintWriter(writer);

        StringBuilder sb = new StringBuilder();

        for (Field method : methods) {
            sb.append('"').append(method.getName());

            if (method.getName().equalsIgnoreCase("id")) {
                sb.append(":ID");
            } else if (method.getType().getName().equalsIgnoreCase("int") || method.getType().getName().startsWith("model.")) {
                sb.append(":int");
            }

            sb.append('"').append(";");
        }

        out.write(sb.substring(0, sb.length() - 1));
        out.write('\n');
        sb.setLength(0);

        Object value;
        String csvValue;

        for (T item : list) {
            for (Field method : methods) {

                value = method.get(item);

                if (value instanceof List) {
                    List<MasterData> values = (List<MasterData>) value;

                    if (values.isEmpty()) {
                        csvValue = "[]";
                    } else {
                        csvValue = getListString(values);
                        writeList(method.getName(), item, values);
                    }

                } else if (value instanceof double[]) {
                    double[] tsData = ((double[]) value);
                    StringBuilder jsonData = new StringBuilder();
                    jsonData.append("[");

                    for (int i = 0; i < tsData.length; i++) {
                        jsonData.append(decimalFormat.format(tsData[i]));
                        jsonData.append(",");
                    }

                    jsonData.deleteCharAt(jsonData.length() - 1);
                    jsonData.append("]");

                    csvValue = jsonData.toString();
                } else if (value instanceof MasterData) {
                    csvValue = String.valueOf(((MasterData) value).id);
                } else {
                    csvValue = String.valueOf(value);
                }

                sb.append('"').append(csvValue).append('"');
                sb.append(";");
            }
            out.write(sb.substring(0, sb.length() - 1));
            out.write('\n');
            sb.setLength(0);
        }

        out.close();
    }

    private static String getListString(List<MasterData> values) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (MasterData item : values) {
            sb.append(item.id);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");

        return sb.toString();
    }

    private static void writeList(String field, MasterData header, List<MasterData> value) throws IOException {
        String headerRow = "";

        File csvFile = new File(path + "/" + header.getClass().getSimpleName() + "_" + field + ".csv");
        if (!csvFile.exists()) {
//            headerRow = String.format("\"%s_id:int\";\"%s_id:int\"\n", header.getClass().getSimpleName(), value.get(0).getClass().getSimpleName());
            headerRow = "\":START_ID\";\":END_ID\"\n";
        }

        FileWriter writer = new FileWriter(csvFile, true);
        PrintWriter out = new PrintWriter(writer);

        if (!headerRow.isEmpty()) {
            out.print(headerRow);
        }

        for (MasterData item : value) {
            out.print(String.format("\"%s\";\"%s\"\n", header.id, item.id));
        }

        out.close();

    }
}
