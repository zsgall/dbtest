package model;

import service.DatabaseCreator;

import static service.DatabaseCreator.getGlobalID;

public class Country extends MasterData {

    public String name;

    public Country(String name) {
        this.id = getGlobalID();
        this.name = name;
    }

    public Country(int id, String name) {
        this.id = id;
        this.name = name;
    }

}
