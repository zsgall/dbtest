package model;

public class TimeSeries extends MasterData {

    public Unit unit;
    public Year year;
    public double[] data;
}
