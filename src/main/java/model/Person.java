package model;

import java.util.Objects;

public class Person extends MasterData {

    public String name;
    public int approvalLimit;

    @Override
    public int hashCode() {
        return Objects.hash(id, name, approvalLimit);
    }
}
