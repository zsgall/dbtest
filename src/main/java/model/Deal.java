package model;

import java.util.List;

public class Deal extends MasterData {

    public Counterparty buyer;
    public Counterparty seller;

    public Person trader;
    public List<Person> approvers;

    public List<Product> products;


}
