package model;

import service.DatabaseCreator;

import static service.DatabaseCreator.getGlobalID;

public class PowerUnit extends Unit {

    public PowerUnit(String name) {
        super(getGlobalID(), name);
    }


    public PowerUnit(int id, String name) {
        super(id, name);
    }
}
