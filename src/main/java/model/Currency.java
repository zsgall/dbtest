package model;

import service.DatabaseCreator;

public class Currency extends Unit {

    public Currency(String name) {
        super(DatabaseCreator.getGlobalID(), name);
    }

    public Currency(int id, String name) {
        super(id, name);
    }
}
