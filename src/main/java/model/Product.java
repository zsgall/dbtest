package model;

public class Product extends MasterData  {

    public String name;
    public Commodity commodity;
    public TimeSeries power;
    public TimeSeries price;
    public Country country;

}
