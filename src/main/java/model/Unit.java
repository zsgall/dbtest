package model;

public abstract class Unit extends MasterData {

    public String name;

    public Unit(int id, String name) {
        this.id = id;
        this.name = name;
    }

}