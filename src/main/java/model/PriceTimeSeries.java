package model;

public class PriceTimeSeries extends TimeSeries {

    public PriceTimeSeries(TimeSeries timeSeries) {
        this.id = timeSeries.id;
        this.year = timeSeries.year;
        this.unit = timeSeries.unit;
        this.data = timeSeries.data;
    }
}
