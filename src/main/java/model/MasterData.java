package model;

import com.google.common.base.Objects;

public abstract class MasterData {

    public int id;

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final MasterData other = (MasterData) obj;
        return Objects.equal(this.id, other.id);
    }
}
