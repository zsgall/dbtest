package model;

import service.DatabaseCreator;

import static service.DatabaseCreator.getGlobalID;

public class Commodity extends MasterData{

    public String name;

    public Commodity(String name) {
        this.id = getGlobalID();
        this.name = name;
    }

    public Commodity(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
