package model;

public class PowerTimeSeries extends TimeSeries {

    public PowerTimeSeries(TimeSeries timeSeries) {
        this.id = timeSeries.id;
        this.year = timeSeries.year;
        this.unit = timeSeries.unit;
        this.data = timeSeries.data;
    }

}
