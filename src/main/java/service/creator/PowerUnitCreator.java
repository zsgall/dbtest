package service.creator;

import model.Currency;
import model.PowerUnit;

import java.util.LinkedList;
import java.util.List;

public class PowerUnitCreator {
    public static List<PowerUnit> create() {
        List<PowerUnit> res = new LinkedList<>();

        res.add(new PowerUnit("KWh"));
        res.add(new PowerUnit("MWh"));
        res.add(new PowerUnit("GWh"));

        return res;
    }
}
