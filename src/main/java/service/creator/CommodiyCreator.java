package service.creator;

import model.Commodity;
import model.Counterparty;
import service.DatabaseCreator;

import java.util.LinkedList;
import java.util.List;

public class CommodiyCreator {
    public static List<Commodity> create() {
        List<Commodity> res = new LinkedList<>();

        res.add(new Commodity("Gas"));
        res.add(new Commodity("Porpane"));
        res.add(new Commodity("Ethenaol"));
        res.add(new Commodity("Brent Curde"));
        res.add(new Commodity("Electricity"));

        return  res;
    }
}
