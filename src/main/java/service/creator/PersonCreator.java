package service.creator;

import model.Person;
import service.DatabaseCreator;

import java.util.LinkedList;
import java.util.List;

import static service.DatabaseCreator.getGlobalID;

public class PersonCreator {
    public static List<Person> create(int numberOfPersons) {
        List<Person> res = new LinkedList<>();
        Person person;

        for (int i = 0; i < numberOfPersons; i++) {
            person = new Person();
            person.id = getGlobalID();
            person.name = String.format("Person #%06d",i);
            res.add(person);
        }

        return res;
    }
}
