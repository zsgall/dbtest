package service.creator;

import com.google.common.base.Objects;
import model.*;
import service.DatabaseCreator;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static service.DatabaseCreator.getGlobalID;

public class ProductCreator {

    private static Random rand = new Random();

    public static List<Product> create(int numberOfProducts, List<Country> countries, List<Commodity> commodities, List<PriceTimeSeries> priceTimeSeries, List<PowerTimeSeries> powerTimeSeries) {
        List<Product> res = new LinkedList<>();

        for (int i = 0; i < numberOfProducts; i++) {
            Product product = new Product();
            product.id = getGlobalID()
            ;
            product.commodity = commodities.get(rand.nextInt(commodities.size()));
            product.power = powerTimeSeries.get(rand.nextInt(powerTimeSeries.size()));
            // jesus... :D
            do {
                product.price = priceTimeSeries.get(rand.nextInt(priceTimeSeries.size()));
            } while (!Objects.equal(product.price.year, product.power.year));
            product.country = countries.get(rand.nextInt(countries.size()));

            product.name = String.format("%d - %s - %s #%06d",
                    product.power.year.yearNumber,
                    product.commodity.name,
                    product.price.unit.name,
                    i);

            res.add(product);

        }

        return res;
    }
}
