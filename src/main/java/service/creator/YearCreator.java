package service.creator;

import model.Year;

import java.util.LinkedList;
import java.util.List;

public class YearCreator {
    public static List<Year> create() {
        List<Year> res = new LinkedList<>();
        Year year;

        for (int i = 0; i < 20; i++) {
            year = new Year();
            year.id = i;
            year.yearNumber = 2000 + i;
            res.add(year);
        }

        return res;
    }
}
