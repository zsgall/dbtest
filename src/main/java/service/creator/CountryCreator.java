package service.creator;

import model.Country;
import model.Currency;

import java.util.LinkedList;
import java.util.List;

public class CountryCreator {

    private static String[] countries = {"Austria","Switzerland","Germany","France","Italy","Belgium","Netherlands","Croatia",
            "Slowenia","Hungary","Slovakia","Czech Republic","Poland","Romania","Serbia","Denmark","Ukraine",
            "Bosnia Herzegovina","Cerna Gora","Albania","Macedonia","Bulgaria","Greece","Great Britain","Spain",
            "Norway","Cyprus","United States","Estonia","Lithuania","Korea","Belorussia","Finland","Turkey",
            "Luxembourg","Ireland","Latvia","Croatia","Sweden","Georgia","Taiwan","Cayman Islands","Singapore",
            "Monaco","China","Indonesia","United Arab Emirates","Portugal","Australia"};

    public static List<Country> create() {
        List<Country> res = new LinkedList<>();

        for (int i = 0; i < countries.length; i++) {
            res.add(new Country(countries[i]));
        }

        return res;
    }

}


