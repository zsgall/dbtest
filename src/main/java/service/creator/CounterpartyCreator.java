package service.creator;

import model.Counterparty;
import service.DatabaseCreator;

import java.util.LinkedList;
import java.util.List;

import static service.DatabaseCreator.getGlobalID;

public class CounterpartyCreator {
    public static List<Counterparty> create(int numberOfCounterparties) {
        List<Counterparty> res = new LinkedList<>();
        Counterparty counterparty;

        for (int i = 0; i < numberOfCounterparties; i++) {
            counterparty = new Counterparty();
            counterparty.id = getGlobalID();
            counterparty.name = String.format("Counterparty #%06d",i);
            res.add(counterparty);
        }

        return res;
    }
}
