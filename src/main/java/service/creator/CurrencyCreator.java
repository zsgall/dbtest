package service.creator;

import model.Currency;

import java.util.LinkedList;
import java.util.List;

public class CurrencyCreator {


    public static List<Currency> create() {
        List<Currency> res = new LinkedList<>();

        res.add(new Currency("HUN"));
        res.add(new Currency("USD"));
        res.add(new Currency("EUR"));
        res.add(new Currency("GBP"));

        return res;
    }
}
