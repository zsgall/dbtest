package service.creator;

import com.google.common.base.Function;
import model.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import com.google.common.collect.Lists;
import service.DatabaseCreator;

import static service.DatabaseCreator.getGlobalID;

public class TimeSeriesCreator {

    private static Random rand = new Random();

    private static List<TimeSeries> create(int count, List<? extends Unit> units, List<Year> years) {
        List<TimeSeries> res = new LinkedList<>();
        TimeSeries timeSeries;

        for (int i = 0; i < count ; i++) {
            timeSeries = new TimeSeries();
            timeSeries.id =  getGlobalID();
            timeSeries.unit = units.get(rand.nextInt(units.size()));
            timeSeries.year = years.get(rand.nextInt(years.size()));
            timeSeries.data = createData();


            res.add(timeSeries);
        }

        return res;
    }

    private static double[] createData() {
        double[] data = new double[365 * 24];

        for (int i = 0; i < data.length; i++) {
            data[i] = rand.nextDouble() * 2000 - 1000;
        }

        return data;
    }

    public static List<PowerTimeSeries> createPower(int count, List<PowerUnit> units, List<Year> years) {

        Function<TimeSeries, PowerTimeSeries> toPower = new Function<TimeSeries, PowerTimeSeries>() {
            @Override
            public PowerTimeSeries apply(TimeSeries timeSeries) {
                return new PowerTimeSeries(timeSeries);
            }
        };

        return Lists.transform(create(count, units, years), toPower) ;
    }

    public static List<PriceTimeSeries> createPrice(int count, List<Currency> units, List<Year> years) {

        Function<TimeSeries, PriceTimeSeries> toPrice = new Function<TimeSeries, PriceTimeSeries>() {
            @Override
            public PriceTimeSeries apply(TimeSeries timeSeries) {
                return new PriceTimeSeries(timeSeries);
            }
        };


        return Lists.transform(create(count, units, years), toPrice) ;
    }
}
