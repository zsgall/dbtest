package service.creator;

import model.Counterparty;
import model.Deal;
import model.Person;
import model.Product;
import service.DatabaseCreator;

import java.util.*;

import static service.DatabaseCreator.getGlobalID;

public class DealCreator {

    private static Random rand = new Random();
    private static List<Person> localPersons;
    private static List<Counterparty> localCounterparties;

    public static List<Deal> create(int count, List<Counterparty> counterparties, List<Person> persons, List<Product> products) {

        List<Deal> res = new LinkedList<>();
        Deal deal;

        for (int i = 0; i < count; i++) {

            if ((i % 1000) == 0) {
                System.out.println( "Done " + Thread.currentThread().getName() + " " + i);
            }

            deal = new Deal();
            localPersons = new ArrayList<>(persons);
            localCounterparties = new ArrayList<>(counterparties);

            deal.id = getGlobalID();
            deal.trader = getPerson();

            List<Person> approvers = new LinkedList<>();
            approvers.add(getPerson());
            approvers.add(getPerson());

            deal.approvers = approvers;

            deal.buyer = getCounterparty();
            deal.seller = getCounterparty();

            deal.products = getProducts(products);

            res.add(deal);
        }

        return res;
    }

    private static List<Product> getProducts(List<Product> products) {
        int cnt = rand.nextInt(4) + 1;
        List<Product> localProds = new ArrayList<>(products);
        List<Product> selectedProducts = new LinkedList<>();

        for (int i = 0; i < cnt; i++) {
            Product product = localProds.get(rand.nextInt(localProds.size()));
            selectedProducts.add(product);
            localProds.remove(product);
        }


        return selectedProducts;
    }

    private static Counterparty getCounterparty() {
        Counterparty counterparty = localCounterparties.get(rand.nextInt(localCounterparties.size()));;
        localCounterparties.remove(counterparty);

        return counterparty;
    }

    private static Person getPerson() {
        Person person = localPersons.get(rand.nextInt(localPersons.size()));;
        localPersons.remove(person);

        return person;
    }


}
