package service;

import model.*;
import service.creator.*;
import uitls.DataFileWriter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DatabaseCreator {

    private static int NUMBER_OF_COUNTERPARTIES = 15000;
    private static int NUMBER_OF_DEALS          = 5000000;
    private static int NUMBER_OF_PERSONS        = 20000;
    private static int NUMBER_OF_PRODUCTS       = 100000;
    private static int NUMBER_OF_TS             = 300;
    private static int NUMBER_OF_THREADS        = 4;

    private static int globalID = 100;

    List<Deal> deals = new ArrayList<>();

    public static void main(String [] args)
    {
        DatabaseCreator databaseCreator = new DatabaseCreator();
        databaseCreator.create(args[0]);
    }

    public void create(String path) {

        final List<Counterparty> counterparties = CounterpartyCreator.create(NUMBER_OF_COUNTERPARTIES);
        List<Commodity> commodities = CommodiyCreator.create();
        List<Currency> currencies = CurrencyCreator.create();
        final List<Person> persons = PersonCreator.create(NUMBER_OF_PERSONS);
        List<PowerUnit> powerUnits = PowerUnitCreator.create();
        List<Year> years = YearCreator.create();
        List<PowerTimeSeries> powerTimeSeries = TimeSeriesCreator.createPower(NUMBER_OF_TS, powerUnits, years);
        List<PriceTimeSeries> priceTimeSeries = TimeSeriesCreator.createPrice(NUMBER_OF_TS, currencies, years);

        List<Country> countries = CountryCreator.create();
        final List<Product> products = ProductCreator.create(NUMBER_OF_PRODUCTS, countries, commodities, priceTimeSeries, powerTimeSeries);

        ExecutorService taskExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    deals.addAll(DealCreator.create(NUMBER_OF_DEALS/NUMBER_OF_THREADS, counterparties, persons, products));
                }
            });
        }

        taskExecutor.shutdown();
        try {
            taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            DataFileWriter.setPath(path);

            DataFileWriter.write(counterparties, Counterparty.class);
            DataFileWriter.write(commodities, Commodity.class);
            DataFileWriter.write(currencies, Currency.class);
            DataFileWriter.write(persons, Person.class);
            DataFileWriter.write(powerUnits, PowerUnit.class);
            DataFileWriter.write(years, Year.class);
            DataFileWriter.write(powerTimeSeries, PowerTimeSeries.class);
            DataFileWriter.write(priceTimeSeries, PriceTimeSeries.class);

            DataFileWriter.write(countries, Country.class);
            DataFileWriter.write(products, Product.class);

            DataFileWriter.write(deals, Deal.class);

            DataFileWriter.createRelations(deals, "trader", "TRADES");
            DataFileWriter.createRelations(deals, "buyer", "BUYS");
            DataFileWriter.createRelations(deals, "seller", "SELLS");

            DataFileWriter.createRelations(products, "commodity", "COMMODITY_TYPE_OF");
            DataFileWriter.createRelations(products, "power", "POWER_OF");
            DataFileWriter.createRelations(products, "price", "PRICE_OF");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }

    synchronized public static int getGlobalID() {
        return globalID++;
    }

}
